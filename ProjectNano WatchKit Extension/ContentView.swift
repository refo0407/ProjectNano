//
//  ContentView.swift
//  ProjectNano WatchKit Extension
//
//  Created by Refa Satya Pramudhito on 16/08/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
