//
//  ProjectNanoApp.swift
//  ProjectNano WatchKit Extension
//
//  Created by Refa Satya Pramudhito on 16/08/21.
//

import SwiftUI

@main
struct ProjectNanoApp: App {
    @SceneBuilder var body: some Scene {
        WindowGroup {
            NavigationView {
                ContentView()
            }
        }

        WKNotificationScene(controller: NotificationController.self, category: "myCategory")
    }
}
