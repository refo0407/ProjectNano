//
//  NotificationView.swift
//  ProjectNano WatchKit Extension
//
//  Created by Refa Satya Pramudhito on 16/08/21.
//

import SwiftUI

struct NotificationView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
